<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Withdraw extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'requested_date', 'amount', 'customer_id', 'currency', 'bank_id', 'account_id', 'account_name',
        'accepted_by_id', 'created_by', 'accepted_date', 'status', 'created_by'
    ];
    protected $table = 'withdraws';

    public function creater()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function acceptor()
    {
        return $this->belongsTo(User::class, 'accepted_by_id');
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id');
    }

    public function customer()
    {
        return $this->belongsTo(Client::class, 'customer_id');
    }
}
