<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use ParseError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
//        dd($exception);
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     */

    public function render($request, Exception $exception)
    {
        if ($request->is('api*')) {
            if ($exception instanceof ValidationException) {
                return response([
                    'status' => 'error',
                    'errors' => $exception->errors()
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            if ($exception instanceof AuthorizationException) {
                return response([
                    'status' => 'error',
                    'message' => $exception->getMessage()
                ], Response::HTTP_FORBIDDEN);
            }

            if ($exception instanceof ModelNotFoundException ||
                $exception instanceof NotFoundHttpException
            ) {
                return response([
                    'status' => 'error',
                    'message' => '404 Not Found.'
                ], Response::HTTP_NOT_FOUND);
            }

            if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
                return response([
                    'status' => 'error',
                    'message' => $exception->getMessage()
                ], 401);
            }
            if ($exception instanceof QueryException) {
                return response([
                    'status' => 'error',
                    'message' => $exception->getMessage()
                ], 401);
            }
            if ($exception instanceof ParseError) {
                return response([
                    'status' => 'error',
                    'message' => $exception->getMessage()
                ], 401);
            }
            if ($exception instanceof BindingResolutionException) {
                return response([
                    'status' => 'error',
                    'message' => $exception->getMessage()
                ], 401);
            }
            if ($exception instanceof \BadMethodCallException) {
                return response([
                    'status' => 'error',
                    'message' => $exception->getMessage()
                ], 401);
            }
            if ($exception instanceof MethodNotAllowedHttpException) {
                return response([
                    'status' => 'error',
                    'message' => $exception->getMessage()
                ], Response::HTTP_METHOD_NOT_ALLOWED);
            }
            if ($exception instanceof \ArgumentCountError) {
                return response([
                    'status' => 'error',
                    'message' => 'Too few arguments to function'
                ], Response::HTTP_BAD_REQUEST);
            }
//            dd($exception);
            return response([
                'status' => 'Error',
                'message' => 'Something Went Wrong'],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        parent::render($request, $exception);
    }
}
