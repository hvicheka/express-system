<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expend extends Model
{
    use SoftDeletes;
    protected $fillable = ['transation_id', 'amount', 'description', 'created_by', 'updated_by'];
    protected $table = 'expends';

    public function creater()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function transation()
    {
        return $this->belongsTo(Transaction::class, 'transation_id');
    }
}
