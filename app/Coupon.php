<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'code', 'discount_amount', 'discount_type', 'amount_from', 'valid_from', 'valid_to', 'description', 'limit_use_number', 'status', 'created_by', 'updated_by'];
    protected $table = 'coupons';

    public function creater()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
