<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'feature_image', 'description', 'status', 'created_by'];
    protected $table = 'banks';

    public function creater()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
