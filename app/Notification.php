<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['id','user_id','type','data','read_at'];
    protected $table = 'notifications';
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
