<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DriverRating extends Model
{
    use SoftDeletes;

    protected $fillable = ['driver_id', 'customer_id', 'star_rating', 'description', 'status'];
    protected $table = 'driver_ratings';

    public function driver()
    {
        return $this->belongsTo(Captain::class, 'driver_id');
    }

    public function customer()
    {
        return $this->belongsTo(Client::class, 'customer_id');
    }
}
