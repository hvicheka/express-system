<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Informing extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'informing_type_id', 'description', 'status', 'created_by'];
    protected $table = 'informings';

    public function creater()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function informing_type()
    {
        return $this->belongsTo(InformingType::class, 'informing_type_id');
    }
}
