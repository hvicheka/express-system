<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InformingType extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'icon', 'status', 'description', 'created_by', 'updated_by'];
    protected $table = 'informing_types';

    public function creater()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function informings()
    {
        return $this->hasMany(informing::class, 'informing_type_id');
    }
}
