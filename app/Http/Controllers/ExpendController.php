<?php

namespace App\Http\Controllers;

use App\Expend;
use App\Shipment;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExpendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expends = Shipment::with(['client'])->paginate(10);
        return view('backend.expends.index', compact('expends'));
    }

    // /**
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function create()
    // {
    //     $transations =  Transaction::select('id', 'value')->get();
    //     return view('backend.expends.create', compact('transations'));
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param \Illuminate\Http\Request $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    //     $request->validate([
    //         'Expends.amount' => 'required',
    //     ]);
    //     $expends = $request->post('Expends');
    //     $expends['created_by'] = Auth::user()->id;
    //     $expends['updated_by'] = Auth::user()->id;
        
    //     if (env('DEMO_MODE') == 'On') {
    //         flash(translate('This action is disabled in demo mode'))->error();
    //         return back();
    //     }
    //     $created_expend = Expend::create($expends);
    //     if ($created_expend) {
    //         flash(translate('Expend history has been inserted successfully'))->success();
    //         return redirect()->route('admin.expends.index');
    //     } else {
    //         flash(translate('Something went wrong'))->error();
    //         return back();
    //     }
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param int $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param int $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit($id)
    // {
    //     if (env('DEMO_MODE') == 'On') {
    //         flash(translate('This action is disabled in demo mode'))->error();
    //         return back();
    //     }
    //     $expend = Expend::findOrFail($id);
    //     $transations =  Transaction::select('id', 'value')->get();
    //     return view('backend.expends.edit', compact('expend', 'transations'));
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param \Illuminate\Http\Request $request
    //  * @param int $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     if (env('DEMO_MODE') == 'On') {
    //         flash(translate('This action is disabled in demo mode'))->error();
    //         return back();
    //     }
    //     $request->validate([
    //         'Expends.amount' => 'required',
    //     ]);
    //     $expend = $request->post('Expends');

    //     $selected_expend = Expend::findOrFail($id);
    //     if ($selected_expend) {
    //         $expend['updated_by'] = Auth::user()->id;
    //         $updated_expend = $selected_expend->update($expend);
    //         if ($updated_expend) {
    //             flash(translate('Expend History has been updated successfully'))->success();
    //             return redirect()->route('admin.expends.index');
    //         } else {
    //             flash(translate('Something went wrong'))->error();
    //             return back();
    //         }
    //     } else {
    //         flash(translate('Something went wrong'))->error();
    //         return back();
    //     }
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param int $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     if (Expend::destroy($id)) {
    //         flash(translate('Expend history has been deleted successfully'))->success();
    //         return redirect()->route('admin.expends.index');
    //     }
    //     flash(translate('Something went wrong'))->error();
    //     return back();
    // }
}
