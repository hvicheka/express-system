<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function index()
    {
        $notifications = Auth::user()->notifications->first();
        return view('backend.notifications.all', compact('notifications') );
    }

    public function notifications()
    {
        $notifications = Auth::user()->notifications;
        return view('backend.notifications.index', compact('notifications') );
    }

    public function notification($id)
    {

        $notification = Auth::user()->notifications()->where('id',$id)->first();
        if($notification->read_at == null){
            $notification->markAsRead();
        }

        return redirect($notification->data['message']['url']);
    }
}
