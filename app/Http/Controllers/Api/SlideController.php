<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Slider;

class SlideController extends Controller
{
    public function index()
    {
        $homeSlide = Slider::query()->where('banner_type', '=', 'home-slide')->get(['id', 'photo', 'url']);
        $homeSlide->map(function ($slide) {
            $slide['photo'] = asset('public/' . $slide['photo']);
        });
        $promotionSlide = Slider::query()->where('banner_type', '=', 'promotion-slide')->get(['id', 'photo', 'url']);
        $promotionSlide->map(function ($slide) {
            $slide['photo'] = asset('public/' . $slide['photo']);
        });
        return response()->json([
            'home_slide' => $homeSlide,
            'promotion_slide' => $promotionSlide
        ]);
    }
}
