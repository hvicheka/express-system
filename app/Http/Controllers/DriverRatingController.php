<?php

namespace App\Http\Controllers;

use App\Captain;
use App\Client;
use App\DriverRating;
use App\Promotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DriverRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $driver_ratings = DriverRating::with(['driver', 'customer'])->paginate(10);
        return view('backend.driver_ratings.index', compact('driver_ratings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $drivers = Captain::select('id', 'name')->get();
        $customers = Client::select('id', 'name')->get();
        return view('backend.driver_ratings.create', compact('drivers', 'customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Ratings.driver_id' => 'required',
            'Ratings.customer_id' => 'required',
            'Ratings.star_rating' => 'required|numeric|min:0|max:127',
        ]);
        $rating = $request->post('Ratings');
        
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $created_rating = DriverRating::create($rating);
        if ($created_rating) {
            flash(translate('Driver Ratings has been inserted successfully'))->success();
            return redirect()->route('admin.driver_ratings.index');
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $drivers = Captain::select('id', 'name')->get();
        $customers = Client::select('id', 'name')->get();
        $rating = DriverRating::findOrFail($id);
        return view('backend.driver_ratings.edit', compact('rating', 'drivers', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $request->validate([
            'Ratings.driver_id' => 'required',
            'Ratings.customer_id' => 'required',
            'Ratings.star_rating' => 'required|numeric|min:0|max:127',
        ]);
        $rating = $request->post('Ratings');

        $selected_rating = DriverRating::findOrFail($id);
        if ($selected_rating) {
            $updated_rating = $selected_rating->update($rating);
            if ($updated_rating) {
                flash(translate('Driver Rating has been updated successfully'))->success();
                return redirect()->route('admin.driver_ratings.index');
            } else {
                flash(translate('Something went wrong'))->error();
                return back();
            }
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (DriverRating::destroy($id)) {
            flash(translate('Driver Rating has been deleted successfully'))->success();
            return redirect()->route('admin.driver_ratings.index');
        }
        flash(translate('Something went wrong'))->error();
        return back();
    }
}
