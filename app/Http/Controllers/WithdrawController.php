<?php

namespace App\Http\Controllers;

use App\Bank;
use App\User;
use App\Client;
use App\Promotion;
use App\Withdraw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $withdraws = Withdraw::with(['creater', 'acceptor', 'bank', 'customer'])->paginate(10);
        return view('backend.withdraws.index', compact('withdraws'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients =  Client::select('id', 'name')->get();
        $banks =  Bank::select('id', 'name')->get();
        $accepters = User::select('id', 'name')->get();

        return view('backend.withdraws.create', compact('clients', 'banks', 'accepters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Withdraws.requested_date' => 'required',
            'Withdraws.amount' => 'required',
            'Withdraws.customer_id' => 'required',
        ]);
        $withdraws = $request->post('Withdraws');
        $withdraws['created_by'] = Auth::user()->id;
        
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $created_withdraw = Withdraw::create($withdraws);
        if ($created_withdraw) {
            flash(translate('Withdraw history has been inserted successfully'))->success();
            return redirect()->route('admin.withdraws.index');
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $withdraw = Withdraw::findOrFail($id);
        $clients =  Client::select('id', 'name')->get();
        $banks =  Bank::select('id', 'name')->get();
        $accepters = User::select('id', 'name')->get();
        return view('backend.withdraws.edit', compact('withdraw', 'clients', 'banks', 'accepters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $request->validate([
            'Withdraws.requested_date' => 'required',
            'Withdraws.amount' => 'required',
            'Withdraws.customer_id' => 'required',
        ]);
        $withdraw = $request->post('Withdraws');

        $selected_withdraw = Withdraw::findOrFail($id);
        if ($selected_withdraw) {
            $withdraw['created_by'] = Auth::user()->id;
            $updated_withdraw = $selected_withdraw->update($withdraw);
            if ($updated_withdraw) {
                flash(translate('Withdraw History has been updated successfully'))->success();
                return redirect()->route('admin.withdraws.index');
            } else {
                flash(translate('Something went wrong'))->error();
                return back();
            }
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Withdraw::destroy($id)) {
            flash(translate('Withdraw history has been deleted successfully'))->success();
            return redirect()->route('admin.withdraws.index');
        }
        flash(translate('Something went wrong'))->error();
        return back();
    }
}
