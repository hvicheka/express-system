<?php

namespace App\Http\Controllers;

use App\Client;
use App\Deposit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deposits = Deposit::with(['creater', 'updater', 'customer'])->paginate(10);
        return view('backend.deposits.index', compact('deposits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers =  Client::select('id', 'name')->get();
        return view('backend.deposits.create', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Deposits.customer_id' => 'required',
            'Deposits.amount' => 'required',
        ]);
        $deposit = $request->post('Deposits');
        $deposit['created_by'] = Auth::user()->id;
        $deposit['updated_by'] = Auth::user()->id;
        
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $created_deposit = Deposit::create($deposit);
        if ($created_deposit) {
            flash(translate('Deposit history has been inserted successfully'))->success();
            return redirect()->route('admin.deposits.index');
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $deposit = Deposit::findOrFail($id);
        $customers =  Client::select('id', 'name')->get();
        return view('backend.deposits.edit', compact('deposit', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $request->validate([
            'Deposits.customer_id' => 'required',
            'Deposits.amount' => 'required',
        ]);
        $deposit = $request->post('Deposits');

        $selected_deposit = Deposit::findOrFail($id);
        if ($selected_deposit) {
            $deposit['updated_by'] = Auth::user()->id;
            $updated_deposit = $selected_deposit->update($deposit);
            if ($updated_deposit) {
                flash(translate('Deposit History has been updated successfully'))->success();
                return redirect()->route('admin.deposits.index');
            } else {
                flash(translate('Something went wrong'))->error();
                return back();
            }
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Deposit::destroy($id)) {
            flash(translate('Deposit history has been deleted successfully'))->success();
            return redirect()->route('admin.deposits.index');
        }
        flash(translate('Something went wrong'))->error();
        return back();
    }
}
