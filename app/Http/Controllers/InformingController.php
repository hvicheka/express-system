<?php

namespace App\Http\Controllers;

use App\Informing;
use App\InformingType;
use App\Promotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InformingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informings = Informing::with(['creater', 'informing_type'])->paginate(10);
        return view('backend.informings.index', compact('informings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $informing_types = InformingType::select('id', 'name')->get();
        return view('backend.informings.create', compact('informing_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Informing.name' => 'required',
            'Informing.informing_type_id' => 'required',
        ]);
        $informing = $request->post('Informing');
        $informing['updated_by'] = Auth::user()->id;
        
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $created_informing = Informing::create($informing);
        if ($created_informing) {
            flash(translate('Notification has been inserted successfully'))->success();
            return redirect()->route('admin.informings.index');
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $informing_types = InformingType::select('id', 'name')->get();
        $informing = Informing::findOrFail($id);
        return view('backend.informings.edit', compact('informing', 'informing_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $request->validate([
            'Informing.name' => 'required',
            'Informing.informing_type_id' => 'required',
        ]);
        $informing = $request->post('Informing');

        $selected_informing = Informing::findOrFail($id);
        if ($selected_informing) {
            $updated_informing = $selected_informing->update($informing);
            if ($updated_informing) {
                flash(translate('Message has been updated successfully'))->success();
                return redirect()->route('admin.informings.index');
            } else {
                flash(translate('Something went wrong'))->error();
                return back();
            }
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Promotion::destroy($id)) {
            flash(translate('Promotion has been deleted successfully'))->success();
            return redirect()->route('admin.promotions.index');
        }
        flash(translate('Something went wrong'))->error();
        return back();
    }
}
