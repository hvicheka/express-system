<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Promotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotions = Promotion::with(['creater', 'updater'])->paginate(10);
        return view('backend.promotions.index', compact('promotions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.promotions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Promotion.name' => 'required',
            'Promotion.amount' => 'required',
        ]);
        $promotion = $request->post('Promotion');
        $promotion['created_by'] = Auth::user()->id;
        $promotion['updated_by'] = Auth::user()->id;
        
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $created_promotion = Promotion::create($promotion);
        if ($created_promotion) {
            flash(translate('Promotion has been inserted successfully'))->success();
            return redirect()->route('admin.promotions.index');
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $promotion = Promotion::findOrFail($id);
        return view('backend.promotions.edit', compact('promotion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $request->validate([
            'Promotion.name' => 'required',
            'Promotion.amount' => 'required',
        ]);
        $promotion = $request->post('Promotion');

        $selected_promotion = Promotion::findOrFail($id);
        if ($selected_promotion) {
            $promotion['updated_by'] = Auth::user()->id;
            $updated_coupon = $selected_promotion->update($promotion);
            if ($updated_coupon) {
                flash(translate('Promotion has been updated successfully'))->success();
                return redirect()->route('admin.promotions.index');
            } else {
                flash(translate('Something went wrong'))->error();
                return back();
            }
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Promotion::destroy($id)) {
            flash(translate('Promotion has been deleted successfully'))->success();
            return redirect()->route('admin.promotions.index');
        }
        flash(translate('Something went wrong'))->error();
        return back();
    }
}
