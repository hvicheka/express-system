<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::with(['creater', 'updater'])->paginate(10);
        return view('backend.coupons.index', compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.coupons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Coupon.name' => 'required',
            'Coupon.code' => 'required',
            'Coupon.discount_amount' => 'required',
            'Coupon.discount_type' => 'required',
            'Coupon.amount_from' => 'required',
            'Coupon.valid_from' => 'required',
            'Coupon.valid_to' => 'required',
        ]);
        $coupon = $request->post('Coupon');
        $coupon['created_by'] = Auth::user()->id;
        $coupon['updated_by'] = Auth::user()->id;
        
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $created_coupon = Coupon::create($coupon);
        if ($created_coupon) {
            flash(translate('Coupon Code has been inserted successfully'))->success();
            return redirect()->route('admin.coupons.index');
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $coupon = Coupon::findOrFail($id);
        return view('backend.coupons.edit', compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $request->validate([
            'Coupon.name' => 'required',
            'Coupon.code' => 'required',
            'Coupon.discount_amount' => 'required',
            'Coupon.discount_type' => 'required',
            'Coupon.amount_from' => 'required',
            'Coupon.valid_from' => 'required',
            'Coupon.valid_to' => 'required',
        ]);
        $coupon = $request->post('Coupon');

        $selected_coupon = Coupon::findOrFail($id);
        if ($selected_coupon) {
            $coupon['updated_by'] = Auth::user()->id;
            $updated_coupon = $selected_coupon->update($coupon);
            if ($updated_coupon) {
                flash(translate('Coupon Code has been updated successfully'))->success();
                return redirect()->route('admin.coupons.index');
            } else {
                flash(translate('Something went wrong'))->error();
                return back();
            }
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Coupon::destroy($id)) {
            flash(translate('Coupon code has been deleted successfully'))->success();
            return redirect()->route('admin.coupons.index');
        }
        flash(translate('Something went wrong'))->error();
        return back();
    }
}
