<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use App\Upload;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slider::query()->orderBy('banner_type', 'ASC')->paginate(10);
        return view('backend.slides.index', ['slides' => $slides]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.slides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $_request = $request->slide;
        $upload = Upload::find($_request['photo']);
        Slider::create([
            'banner_type' => $_request['banner_type'],
            'photo' => $upload->file_name ?? '',
            'published' => 1
        ]);
        flash(translate("Slider added successfully"))->success();
        return redirect()->route('slides.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slider = Slider::findOrFail($id);
        return view('backend.slides.show', ['slide' => $slider]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        return view('backend.slides.edit', ['slide' => $slider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $_request = $request->slide;
        $slider = Slider::findOrFail($id);
        if (!empty($_request['photo'])) {
            $upload = Upload::find($_request['photo']);
            $fileName = $upload->file_name;
        } else {
            $fileName = $slider->photo;
        }
        $slider->update([
            'banner_type' => $_request['banner_type'],
            'photo' => $fileName,
            'published' => 1
        ]);
        flash(translate("Slider update successfully"))->success();
        return redirect()->route('slides.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);
        $slider->delete();
        if (\request()->ajax()) {
            return response()->json([
                'success' => true,
                'id' => $id
            ]);
        }
        flash(translate("Slider deleted successfully"))->success();
        return redirect()->route('slides.index');
    }
}
