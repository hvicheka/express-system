<?php

namespace App\Http\Controllers;

use App\Client;
use App\ClientMention;
use App\Deposit;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientMentionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client_mentions = ClientMention::with(['transation', 'customer'])->paginate(10);
        return view('backend.client_mentions.index', compact('client_mentions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers =  Client::select('id', 'name')->get();
        $transations =  Transaction::select('id', 'value')->get();
        return view('backend.client_mentions.create', compact('customers', 'transations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'ClientMentions.transation_id' => 'required',
            'ClientMentions.customer_id' => 'required',
            'ClientMentions.amount' => 'required',
        ]);
        $client_mention = $request->post('ClientMentions');
        
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $created_client_mention = ClientMention::create($client_mention);
        if ($created_client_mention) {
            flash(translate('Client Mentioned has been inserted successfully'))->success();
            return redirect()->route('admin.client_mentions.index');
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $client_mention = ClientMention::findOrFail($id);
        $customers =  Client::select('id', 'name')->get();
        $transations =  Transaction::select('id', 'value')->get();
        return view('backend.client_mentions.edit', compact('client_mention', 'customers', 'transations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (env('DEMO_MODE') == 'On') {
            flash(translate('This action is disabled in demo mode'))->error();
            return back();
        }
        $request->validate([
            'ClientMentions.transation_id' => 'required',
            'ClientMentions.customer_id' => 'required',
            'ClientMentions.amount' => 'required',
        ]);
        $client_mention = $request->post('ClientMentions');

        $selected_client_mention = ClientMention::findOrFail($id);
        if ($selected_client_mention) {
            $updated_deposit = $selected_client_mention->update($client_mention);
            if ($updated_deposit) {
                flash(translate('Client Mentioned has been updated successfully'))->success();
                return redirect()->route('admin.client_mentions.index');
            } else {
                flash(translate('Something went wrong'))->error();
                return back();
            }
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (ClientMention::destroy($id)) {
            flash(translate('Client mentioned has been deleted successfully'))->success();
            return redirect()->route('admin.client_mentions.index');
        }
        flash(translate('Something went wrong'))->error();
        return back();
    }
}
