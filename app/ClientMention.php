<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientMention extends Model
{
    use SoftDeletes;
    protected $fillable = ['transation_id', 'customer_id', 'amount', 'note'];
    protected $table = 'client_mentions';

    public function customer()
    {
        return $this->belongsTo(Client::class, 'customer_id');
    }

    public function transation()
    {
        return $this->belongsTo(Transaction::class, 'transation_id');
    }
}
