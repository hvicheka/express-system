<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'amount', 'promotion_type', 'discount_type', 'from_date', 'to_date', 'description', 'status', 'created_by', 'updated_by'];
    protected $table = 'promotions';

    public function creater()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
