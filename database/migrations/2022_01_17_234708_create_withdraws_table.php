<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('feature_image')->nullable();
            $table->text('description')->nullable();
            $table->enum('status', ['active', 'inactive']);
            $table->unsignedInteger('created_by');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('withdraws', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('requested_date');
            $table->double('amount');
            $table->unsignedInteger('customer_id');
            $table->enum('currency', ['dollar', 'riel']);
            $table->unsignedInteger('bank_id');
            $table->string('account_id')->nullable();
            $table->string('account_name')->nullable();
            $table->unsignedInteger('accepted_by_id')->nullable();
            $table->date('accepted_date')->nullable();
            $table->unsignedInteger('created_by');
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraws');
        Schema::dropIfExists('banks');
    }
}
