<?php  

use Illuminate\Support\Facades\Route;

Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'user_role:admin|staff']], function() {
    Route::get('notifications/delete/{id}','NotificationController@destroy')->name('admin.notification.delete');
    
    Route::resource('notifications','NotificationController',[
        'as' => 'admin'
    ]);
});