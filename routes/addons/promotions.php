<?php  

use Illuminate\Support\Facades\Route;

Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'user_role:admin|staff']], function() {
    //Update Routes
    Route::get('promotion/delete/{id}','PromotionController@destroy')->name('admin.promotions.delete');
    
    Route::resource('promotions','PromotionController',[
        'as' => 'admin'
    ]);
});