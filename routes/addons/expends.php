<?php  

use Illuminate\Support\Facades\Route;

Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'user_role:admin|staff']], function() {
    //Update Routes
    // Route::get('expends/delete/{id}','ExpendController@destroy')->name('admin.expends.delete');
    
    // Route::resource('expends','ExpendController',[
    //     'as' => 'admin'
    // ]);
    Route::get('expends', 'ExpendController@index')->name('admin.expends.index');
});