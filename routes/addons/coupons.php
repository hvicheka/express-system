<?php  

use Illuminate\Support\Facades\Route;

Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'user_role:admin|staff']], function() {
    //Update Routes
    Route::get('coupons/delete/{id}','CouponController@destroy')->name('admin.coupons.delete');
    
    Route::resource('coupons','CouponController',[
        'as' => 'admin'
    ]);
});