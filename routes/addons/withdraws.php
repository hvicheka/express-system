<?php  

use Illuminate\Support\Facades\Route;

Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'user_role:admin|staff']], function() {
    //Update Routes
    Route::get('withdraws/delete/{id}','WithdrawController@destroy')->name('admin.withdraws.delete');
    
    Route::resource('withdraws','WithdrawController',[
        'as' => 'admin'
    ]);
});