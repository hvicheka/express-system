<?php  

use Illuminate\Support\Facades\Route;

Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'user_role:admin|staff']], function() {
    Route::get('informings/delete/{id}','InformingController@destroy')->name('admin.informings.delete');
    
    Route::resource('informings','InformingController',[
        'as' => 'admin'
    ]);
});