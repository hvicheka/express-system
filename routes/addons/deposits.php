<?php  

use Illuminate\Support\Facades\Route;

Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'user_role:admin|staff']], function() {
    //Update Routes
    Route::get('deposits/delete/{id}','DepositController@destroy')->name('admin.deposits.delete');
    
    Route::resource('deposits','DepositController',[
        'as' => 'admin'
    ]);
});