<?php  

use Illuminate\Support\Facades\Route;

Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'user_role:admin|staff']], function() {
    //Update Routes
    Route::get('driver-rating/delete/{id}','DriverRatingController@destroy')->name('admin.driver_ratings.delete');
    
    Route::resource('driver_ratings','DriverRatingController',[
        'as' => 'admin'
    ]);
});