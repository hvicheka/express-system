<?php  

use Illuminate\Support\Facades\Route;

Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'user_role:admin|staff']], function() {
    //Update Routes
    Route::get('client-mention/delete/{id}','ClientMentionController@destroy')->name('admin.client_mentions.delete');
    
    Route::resource('client_mentions','ClientMentionController',[
        'as' => 'admin'
    ]);
});