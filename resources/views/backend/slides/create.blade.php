@extends('backend.layouts.app')

@section('content')

    <div class="mx-auto col-lg-12">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0 h6">{{translate('Add Slide')}}</h5>
            </div>

            <form class="form-horizontal" action="{{ route('slides.store') }}" id="kt_form_1" method="POST"
                  enctype="multipart/form-data">
                @csrf
                {!!redirect_input()!!}
                <div class="card-body">
                    <div class="form-group">
                        <label>{{translate('Type')}}:</label>
                        <select name="slide[banner_type]"
                                class="change-country-client-address form-control select-country">
                            <option value="home-slide">Home Slide</option>
                            <option value="promotion-slide">Promotion Slide</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{translate('Image')}}:</label>

                                <div class="input-group " data-toggle="aizuploader" data-type="image">
                                    <div class="input-group-prepend">
                                        <div
                                            class="input-group-text bg-soft-secondary font-weight-medium">{{ translate('Browse') }}</div>
                                    </div>
                                    <div class="form-control file-amount">{{ translate('Choose File') }}</div>
                                    <input type="hidden" name="slide[photo]" class="selected-files"
                                           value="{{old('photo')}}" required>
                                </div>
                                <div class="file-preview">
                                </div>
                            </div>
                        </div>
                    </div>

                    {!! hookView('spot-cargo-shipment-branch-addon',$currentView) !!}

                    <div class="mb-0 text-right form-group">
                        <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            FormValidation.formValidation(
                document.getElementById('kt_form_1'), {
                    fields: {
                        "slide[banner_type]": {
                            validators: {
                                notEmpty: {
                                    message: '{{translate("This is required!")}}'
                                }
                            }
                        },
                        "slide[photo]": {
                            validators: {
                                notEmpty: {
                                    message: '{{translate("This is required!")}}'
                                }
                            }
                        }
                    },


                    plugins: {
                        autoFocus: new FormValidation.plugins.AutoFocus(),
                        trigger: new FormValidation.plugins.Trigger(),
                        // Bootstrap Framework Integration
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        // Validate fields when clicking the Submit button
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        // Submit the form when all fields are valid
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'fa fa-check',
                            invalid: 'fa fa-times',
                            validating: 'fa fa-refresh',
                        }),
                    }
                }
            );
        });
    </script>
@endsection
