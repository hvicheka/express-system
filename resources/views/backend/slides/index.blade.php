@extends('backend.layouts.app')

@section('content')

    <div class="aiz-titlebar text-left mt-2 mb-3">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h1 class="h3">{{translate('All Slides')}}</h1>
            </div>
            <div class="col-md-6 text-md-right">
                <a href="{{ route('slides.create') }}" class="btn btn-circle btn-info">
                    <span>{{translate('Add New Slide')}}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Slide')}}</h5>
        </div>
        <div class="card-body">
            <table class="table aiz-table mb-0">
                <thead>
                <tr>
                    <th width="3%">#</th>
                    <th>{{translate('Name')}}</th>
                    <th>{{translate('Type')}}</th>
                    <th width="10%" class="text-center">{{translate('Options')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($slides as $key => $slide)

                    <tr data-id="{{$slide->id}}">
                        <td width="3%">{{ ++$key }}</td>
                        <td width="20%">
                            <img class="img-thumbnail" width="70" src="{{ asset('public/' . $slide->photo) }}"
                                 alt="Image">
                        </td>
                        <td width="20%">
                            @if($slide->banner_type === 'home-slide')
                                Home Slide
                            @elseif($slide->banner_type === 'promotion-slide')
                                Promotion Slide
                            @else
                                -
                            @endif
                        </td>

                        <td class="text-center d-flex">
                            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm"
                               href="{{route('slides.show', $slide->id)}}" title="{{ translate('Show') }}">
                                <i class="las la-eye"></i>
                            </a>
                            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm"
                               href="{{route('slides.edit', $slide->id)}}" title="{{ translate('Edit') }}">
                                <i class="las la-edit"></i>
                            </a>
                            {{--                            <form action="{{route('slides.destroy',$slide->id)}}" class="d-flex" method="POST">--}}
                            {{--                                @csrf--}}
                            {{--                                @method('DELETE')--}}
                            {{--                                <button class="btn btn-soft-danger btn-icon btn-circle btn-sm" onclick="e.preventDefault()--}}
                            {{--                                if(confirm('Are you sure?')){--}}
                            {{--                                    alert('ok')--}}
                            {{--                                }--}}
                            {{--"--}}
                            {{--                                ><i class="las la-trash"></i></button>--}}
                            {{--                            </form>--}}
                            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete"
                               data-href="{{route('slides.destroy',$slide->id)}}"
                               title="{{ translate('Delete') }}">
                                <i class="las la-trash"></i>
                            </a>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
            <div class="aiz-pagination">
                {{ $slides->appends(request()->input())->links() }}
            </div>
        </div>
    </div>

@endsection

@section('modal')
    <!-- delete Modal -->
    <div id="delete-modal" class="modal fade">
        <div class="modal-dialog modal-sm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title h6">{{translate('Delete Confirmation')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body text-center">
                    <p class="mt-1">{{translate('Are you sure to delete this?')}}</p>
                    <button type="button" class="btn btn-link mt-2"
                            data-dismiss="modal">{{translate('Cancel')}}</button>
                    <a href="" id="delete-link" class="btn btn-primary mt-2">{{translate('Delete')}}</a>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->
@endsection

@push('script')
    <script>
        $('#delete-link').unbind().click(function (e) {
            e.preventDefault()
            $.post($(this).attr('href'), {_token: '{{ @csrf_token() }}', _method: "DELETE"})
                .done((response) => {
                    if (response.success) {
                        $(`[data-id="${response.id}"]`).remove()
                        $('#delete-modal').modal('hide')
                    }
                })
                .fail(function () {

                })
        })
    </script>
@endpush
