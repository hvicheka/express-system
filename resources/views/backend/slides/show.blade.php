@extends('backend.layouts.app')

@section('content')

    <div class="mx-auto col-lg-12">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0 h6">Slide Type:  {{ $slide->banner_type == 'home-slide' ?  'Home Slide'  : 'Promotion Slide'}} </h5>
            </div>


            <div class="card-body">
                <div class="row">
                    <img class="img-fluid" src="{{ asset('public/' .$slide->photo) }}" alt="Image">
                </div>


            </div>

            @endsection

            @section('script')
                <script type="text/javascript">
                    $(document).ready(function () {
                        FormValidation.formValidation(
                            document.getElementById('kt_form_1'), {
                                fields: {
                                    "slide[banner_type]": {
                                        validators: {
                                            notEmpty: {
                                                message: '{{translate("This is required!")}}'
                                            }
                                        }
                                    },
                                    "slide[photo]": {
                                        validators: {
                                            notEmpty: {
                                                message: '{{translate("This is required!")}}'
                                            }
                                        }
                                    }
                                },


                                plugins: {
                                    autoFocus: new FormValidation.plugins.AutoFocus(),
                                    trigger: new FormValidation.plugins.Trigger(),
                                    // Bootstrap Framework Integration
                                    bootstrap: new FormValidation.plugins.Bootstrap(),
                                    // Validate fields when clicking the Submit button
                                    submitButton: new FormValidation.plugins.SubmitButton(),
                                    // Submit the form when all fields are valid
                                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                                    icon: new FormValidation.plugins.Icon({
                                        valid: 'fa fa-check',
                                        invalid: 'fa fa-times',
                                        validating: 'fa fa-refresh',
                                    }),
                                }
                            }
                        );
                    });
                </script>
@endsection
