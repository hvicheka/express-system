@extends('backend.layouts.app')

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{translate('All Notifications')}}</h1>
		</div>
		<div class="col-md-6 text-md-right">
			<a href="{{ route('admin.promotions.create') }}" class="btn btn-circle btn-info">
				<span>{{translate('Add Notification')}}</span>
			</a>
		</div>
	</div>
</div>

<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{translate('Notification')}}</h5>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th  width="3%">#</th>
                    <th >{{translate('Name')}}</th>
                    <th >{{translate('Notification Type')}}</th>
                    <th >{{translate('Status')}}</th>
                    <th >{{translate('Created By')}}</th>
                    
                    <th  width="10%" class="text-center">{{translate('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($informings as $key => $informing)
                    
                        <tr>
                            <td  width="3%">{{ ($key+1) + ($informings->currentPage() - 1)*$informings->perPage() }}</td>
                            <td width="20%">{{$informing->name}}</td>
                            <td width="20%">{{$informing->informing_type->name}}</td>
                            <td width="20%">{{$informing->status}}</td>
                            <td width="20%">{{$informing->creater->name}}</td>
                           
                            <td class="text-center d-flex">
		                            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('admin.informings.edit', $informing->id)}}" title="{{ translate('Edit') }}">
		                                <i class="las la-edit"></i>
		                            </a>
		                            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('admin.informings.delete', ['id'=> $informing->id])}}" title="{{ translate('Delete') }}">
		                                <i class="las la-trash"></i>
		                            </a>
		                        </td>
                        </tr>
               
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $informings->appends(request()->input())->links() }}
        </div>
    </div>
</div>
{!! hookView('spot-cargo-shipment-client-addon',$currentView) !!}

@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection
