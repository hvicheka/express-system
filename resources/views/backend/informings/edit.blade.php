@extends('backend.layouts.app')

@section('content')
<div class="mx-auto col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Customer Information')}}</h5>
        </div>

        <form class="form-horizontal" action="{{ route('admin.informings.update',['informing'=> $informing->id]) }}" method="POST">
            @csrf
            {{ method_field('PATCH') }}
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Name')}}:</label>
                            <input type="text" class="form-control" placeholder="{{translate('Name')}}" name="Informing[name]" value="{{ $informing->name }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Notification Type')}}:</label>
                            <select class="form-control kt-select2 branch" name="Informing[informing_type_id]">
                                @foreach($informing_types as $informing_type)
                                <option {{ $informing_type->id == $informing->informing_type_id ? 'selected' : '' }} value="{{$informing_type->id}}">{{$informing_type->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Status')}}:</label>
                            <div class="d-flex">
                                <div class="form-check mr-11">
                                    <input class="form-check-input" type="radio" name="Informing[status]" value="active" id="active" {{ $informing->status == 'active' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="active">Active</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="Informing[status]" value="inactive" id="inactive" {{ $informing->status == 'inactive' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inactive">Inactive</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Description')}}:</label>
                            <textarea rows="5" class="form-control" placeholder="{{translate('Description')}}" name="Informing[description]">{{ $informing->description }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="mb-0 text-right form-group">
                    <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection