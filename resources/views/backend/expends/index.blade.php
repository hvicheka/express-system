@extends('backend.layouts.app')

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{translate('All Exends Histories')}}</h1>
		</div>
		{{-- <div class="col-md-6 text-md-right">
			<a href="{{ route('admin.expends.create') }}" class="btn btn-circle btn-info">
				<span>{{translate('Add Expend')}}</span>
			</a>
		</div> --}}
	</div>
</div>

<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{translate('Expend')}}</h5>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th  width="3%">#</th>
                    <th >{{translate('Shipment Code')}}</th>
                    <th >{{translate('Customer Name')}}</th>
                    <th >{{translate('Total Cost')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($expends as $key => $expend)
                    <tr>
                        <td  width="3%">{{ ($key+1) + ($expends->currentPage() - 1)*$expends->perPage() }}</td>
                        <td width="20%">{{$expend->code}}</td>
                        <td width="20%">{{$expend->client->name}}</td>
                        <td width="20%">{{number_format($expend->shipping_cost, 2)}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $expends->appends(request()->input())->links() }}
        </div>
    </div>
</div>
{!! hookView('spot-cargo-shipment-client-addon',$currentView) !!}

@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection
