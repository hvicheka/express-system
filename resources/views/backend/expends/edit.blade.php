@extends('backend.layouts.app')

@section('content')
<div class="mx-auto col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Customer Information')}}</h5>
        </div>

        <form class="form-horizontal" action="{{ route('admin.expends.update',['expend'=> $expend->id]) }}" method="POST">
            @csrf
            {{ method_field('PATCH') }}
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Transation No')}}:</label>
                            <select class="form-control kt-select2 branch" name="Expends[transation_id]">
                                @foreach($transations as $transation)
                                <option {{ $expend->transation_id === $transation->id ? 'selected' : '' }} value="{{$transation->id}}">{{$transation->value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Amount')}}:</label>
                            <input type="number" class="form-control" placeholder="{{translate('Amount')}}" name="Expends[amount]" value="{{ $expend->amount }}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{translate('Description')}}:</label>
                            <textarea rows="5" class="form-control" placeholder="{{translate('Description')}}" name="Expends[description]">{{ $expend->description }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="mb-0 text-right form-group">
                    <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection