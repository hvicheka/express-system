@extends('backend.layouts.app')

@section('content')
<div class="mx-auto col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Withdraw History Information')}}</h5>
        </div>

        <form class="form-horizontal" action="{{ route('admin.withdraws.update',['withdraw'=> $withdraw->id]) }}" method="POST">
            @csrf
            {{ method_field('PATCH') }}
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Requested Date')}}:</label>
                            <input type="date" class="form-control" placeholder="{{translate('Requested Date')}}" name="Withdraws[requested_date]" value="{{ $withdraw->requested_date }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Amount')}}:</label>
                            <input type="number" class="form-control" placeholder="{{translate('Amount')}}" name="Withdraws[amount]" value="{{ $withdraw->amount }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Customer Name')}}:</label>
                            <select class="form-control kt-select2 branch" name="Withdraws[customer_id]">
                                @foreach($clients as $client)
                                <option {{ $withdraw->customer_id == $client->id ? 'selected' : '' }} value="{{$client->id}}">{{$client->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Bank Name')}}:</label>
                            <select class="form-control kt-select2 branch" name="Withdraws[bank_id]">
                                @foreach($banks as $bank)
                                <option {{ $withdraw->bank_id == $bank->id ? 'selected' : '' }} value="{{$bank->id}}">{{$bank->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>{{translate('Currency')}}:</label>
                            <div class="d-flex">
                                <div class="form-check mr-11">
                                    <input class="form-check-input" type="radio" name="Withdraws[currency]" value="dollar" id="dollar" {{ $withdraw->currency == 'dollar' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="dollar">Dollar</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="Withdraws[currency]" value="riel" id="riel" {{ $withdraw->currency == 'riel' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="riel">Riel</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>{{translate('Status')}}:</label>
                            <div class="d-flex">
                                <div class="form-check mr-11">
                                    <input class="form-check-input" type="radio" name="Withdraws[status]" value="request" id="request" {{ $withdraw->status == 'request' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="request">Request</label>
                                </div>
                                <div class="form-check mr-11">
                                    <input class="form-check-input" type="radio" name="Withdraws[status]" value="reject" id="reject" {{ $withdraw->status == 'reject' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="reject">Reject</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="Withdraws[status]" value="accept" id="accept" {{ $withdraw->status == 'accept' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="accept">Accept</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>{{translate('Accepted Date')}}:</label>
                            <input type="date" class="form-control" placeholder="{{translate('Requested Date')}}" name="Withdraws[accepted_date]" value="{{ $withdraw->accepted_date }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>{{translate('Accepted By')}}:</label>
                            <select class="form-control kt-select2 branch" name="Withdraws[accepted_by_id]">
                                @foreach($accepters as $accepter)
                                <option {{ $withdraw->accepted_by_id === $accepter->id ? 'selected' : '' }} value="{{$accepter->id}}">{{$accepter->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Account No')}}:</label>
                            <input type="text" class="form-control" placeholder="{{translate('Account No')}}" name="Withdraws[account_id]" value="{{ $withdraw->account_id }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Account Name')}}:</label>
                            <input type="text" class="form-control" placeholder="{{translate('Account Name')}}" name="Withdraws[account_name]" value="{{ $withdraw->account_name }}">
                        </div>
                    </div>
                </div>
                <div class="mb-0 text-right form-group">
                    <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection