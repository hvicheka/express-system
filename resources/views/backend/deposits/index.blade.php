@extends('backend.layouts.app')

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{translate('All Deposit Histories')}}</h1>
		</div>
		<div class="col-md-6 text-md-right">
			<a href="{{ route('admin.deposits.create') }}" class="btn btn-circle btn-info">
				<span>{{translate('Add Deposit')}}</span>
			</a>
		</div>
	</div>
</div>

<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{translate('Deposit')}}</h5>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th  width="3%">#</th>
                    <th >{{translate('Customer Name')}}</th>
                    <th >{{translate('Amount')}}</th>
                    <th >{{translate('Created By')}}</th>
                    <th >{{translate('Updated By')}}</th>
                    <th  width="10%" class="text-center">{{translate('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($deposits as $key => $deposit)
                        <tr>
                            <td  width="3%">{{ ($key+1) + ($deposits->currentPage() - 1)*$deposits->perPage() }}</td>
                            <td width="20%">{{$deposit->customer->name}}</td>
                            <td width="20%">{{number_format($deposit->amount, 2)}}</td>
                            <td width="20%">{{$deposit->creater->name}}</td>
                            <td width="20%">{{$deposit->updater->name}}</td>
                           
                            <td class="text-center d-flex">
		                            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('admin.deposits.edit', $deposit->id)}}" title="{{ translate('Edit') }}">
		                                <i class="las la-edit"></i>
		                            </a>
		                            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('admin.deposits.delete', ['id'=> $deposit->id])}}" title="{{ translate('Delete') }}">
		                                <i class="las la-trash"></i>
		                            </a>
		                        </td>
                        </tr>
               
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $deposits->appends(request()->input())->links() }}
        </div>
    </div>
</div>
{!! hookView('spot-cargo-shipment-client-addon',$currentView) !!}

@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection
