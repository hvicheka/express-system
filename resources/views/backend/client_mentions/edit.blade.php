@extends('backend.layouts.app')

@section('content')
<div class="mx-auto col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Client Mention Information')}}</h5>
        </div>

        <form class="form-horizontal" action="{{ route('admin.client_mentions.update',['client_mention'=> $client_mention->id]) }}" method="POST">
            @csrf
            {{ method_field('PATCH') }}
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Customer Name')}}:</label>
                            <select class="form-control kt-select2 branch" name="ClientMentions[customer_id]">
                                @foreach($customers as $customer)
                                <option {{ $client_mention->customer_id === $customer->id ? 'selected' : '' }} value="{{$customer->id}}">{{$customer->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Transation Value')}}:</label>
                            <select class="form-control kt-select2 branch" name="ClientMentions[transation_id]">
                                @foreach($transations as $transation)
                                <option {{ $client_mention->transation_id === $transation->id ? 'selected' : '' }} value="{{$transation->id}}">{{$transation->value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Amount')}}:</label>
                            <input type="number" class="form-control" placeholder="{{translate('Amount')}}" name="ClientMentions[amount]" value="{{ $client_mention->amount }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Note')}}:</label>
                            <textarea rows="5" class="form-control" placeholder="{{translate('Note')}}" name="ClientMentions[note]">{{ $client_mention->note }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="mb-0 text-right form-group">
                    <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection