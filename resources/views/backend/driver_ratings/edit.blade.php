@extends('backend.layouts.app')

@section('content')
<div class="mx-auto col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Customer Information')}}</h5>
        </div>

        <form class="form-horizontal" action="{{ route('admin.driver_ratings.update',['driver_rating'=> $rating->id]) }}" method="POST">
            @csrf
            {{ method_field('PATCH') }}
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Driver Name')}}:</label>
                            <select class="form-control kt-select2 branch" name="Ratings[driver_id]">
                                @foreach($drivers as $driver)
                                <option {{ $driver->id == $rating->driver_id ? 'selected' : '' }} value="{{$driver->id}}">{{$driver->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Customer Name')}}:</label>
                            <select class="form-control kt-select2 branch" name="Ratings[customer_id]">
                                @foreach($customers as $customer)
                                <option {{ $customer->id == $rating->customer_id ? 'selected' : '' }} value="{{$customer->id}}">{{$customer->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>{{translate('Status')}}:</label>
                            <div class="d-flex">
                                <div class="form-check mr-11">
                                    <input class="form-check-input" type="radio" name="Ratings[status]" value="active" id="active" {{ $rating->status == 'active' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="active">Active</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="Ratings[status]" value="inactive" id="inactive" {{ $rating->status === 'inactive' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inactive">Inactive</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>{{translate('Rating')}}:</label>
                            <input class="form-control" placeholder="{{translate('Rating')}}" type="number" name="Ratings[star_rating]" value="{{ $rating->star_rating }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Description')}}:</label>
                            <textarea rows="5" class="form-control" placeholder="{{translate('Description')}}" name="Ratings[description]">{{ $rating->description }}</textarea>
                        </div> 
                    </div>
                </div>
                <div class="mb-0 text-right form-group">
                    <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection