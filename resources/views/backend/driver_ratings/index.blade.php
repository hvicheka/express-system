@extends('backend.layouts.app')

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{translate('All Driver Ratings')}}</h1>
		</div>
		<div class="col-md-6 text-md-right">
			<a href="{{ route('admin.driver_ratings.create') }}" class="btn btn-circle btn-info">
				<span>{{translate('Add Driver Rating')}}</span>
			</a>
		</div>
	</div>
</div>

<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{translate('Driver Rating')}}</h5>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th  width="3%">#</th>
                    <th >{{translate('Driver Name')}}</th>
                    <th >{{translate('Customer Name')}}</th>
                    <th >{{translate('Rating')}}</th>
                    <th >{{translate('Status')}}</th>
                    
                    <th  width="10%" class="text-center">{{translate('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($driver_ratings as $key => $rating)
                        <tr>
                            <td  width="3%">{{ ($key+1) + ($driver_ratings->currentPage() - 1)*$driver_ratings->perPage() }}</td>
                            <td width="20%">{{$rating->driver->name}}</td>
                            <td width="20%">{{$rating->customer->name}}</td>
                            <td width="20%">{{$rating->star_rating}}</td>
                            <td width="20%">{{$rating->status}}</td>
                           
                            <td class="text-center d-flex">
		                            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('admin.driver_ratings.edit', $rating->id)}}" title="{{ translate('Edit') }}">
		                                <i class="las la-edit"></i>
		                            </a>
		                            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('admin.driver_ratings.delete', ['id'=> $rating->id])}}" title="{{ translate('Delete') }}">
		                                <i class="las la-trash"></i>
		                            </a>
		                        </td>
                        </tr>
               
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $driver_ratings->appends(request()->input())->links() }}
        </div>
    </div>
</div>
{!! hookView('spot-cargo-shipment-client-addon',$currentView) !!}

@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection
