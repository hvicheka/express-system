@extends('backend.layouts.app')

@section('content')
<div class="mx-auto col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Coupon Code Information')}}</h5>
        </div>

        <form class="form-horizontal" action="{{ route('admin.coupons.update',['coupon'=> $coupon->id]) }}" method="POST">
            @csrf
            {{ method_field('PATCH') }}
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Name')}}:</label>
                            <input type="text" id="name" class="form-control" placeholder="{{translate('Name')}}" value="{{ $coupon->name }}" name="Coupon[name]">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Code')}}:</label>
                            <input type="text" class="form-control" placeholder="{{translate('Code')}}" value="{{ $coupon->code }}" name="Coupon[code]">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Discount Amount')}}:</label>
                            <input type="number" id="name" class="form-control" placeholder="{{translate('Discount Amount')}}" value="{{ $coupon->discount_amount }}" name="Coupon[discount_amount]">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Discount Type')}}:</label>
                            <div class="d-flex">
                                <div class="form-check mr-11">
                                    <input class="form-check-input" type="radio" name="Coupon[discount_type]" value="percentage" id="percentage" {{ $coupon->discount_type == 'percentage' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="percentage">%</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="Coupon[discount_type]" value="amount" id="amount" {{ $coupon->discount_type == 'amount' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="amount">$</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Amount From')}}:</label>
                            <input type="number" class="form-control" placeholder="{{translate('Amount From')}}" name="Coupon[amount_from]" value="{{ $coupon->amount_from }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Limit use number')}}:</label>
                            <input type="number" class="form-control" placeholder="{{translate('Limit use number')}}" name="Coupon[limit_use_number]" value="{{ $coupon->limit_use_number }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Valid From')}}:</label>
                            <input type="date" class="form-control" placeholder="{{translate('Starting From')}}" name="Coupon[valid_from]" value="{{ $coupon->valid_from }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Valid To')}}:</label>
                            <input type="date" class="form-control" placeholder="{{translate('Starting To')}}" name="Coupon[valid_to]" value="{{ $coupon->valid_to }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Status')}}:</label>
                            <input type="type" class="form-control" placeholder="{{translate('Status')}}" name="Coupon[status]" value="{{ $coupon->status }}">
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Status')}}:</label>
                            <div class="d-flex">
                                <div class="form-check mr-11">
                                    <input class="form-check-input" type="radio" name="Coupon[status]" value="active" id="active" {{ $coupon->status == 'active' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="active">Active</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="Coupon[status]" value="inactive" id="inactive" {{ $coupon->status =='inactive' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inactive">Inactive</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Description')}}:</label>
                            <textarea rows="5" class="form-control" placeholder="{{translate('Description')}}" name="Coupon[description]">{{ $coupon->description }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="mb-0 text-right form-group">
                    <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection