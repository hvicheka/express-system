@extends('backend.layouts.app')

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{translate('All Coupon Codes')}}</h1>
		</div>
		<div class="col-md-6 text-md-right">
			<a href="{{ route('admin.coupons.create') }}" class="btn btn-circle btn-info">
				<span>{{translate('Add New Coupon Codea')}}</span>
			</a>
		</div>
	</div>
</div>

<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{translate('Coupon Code')}}</h5>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th  width="3%">#</th>
                    <th >{{translate('Name')}}</th>
                    <th >{{translate('Code')}}</th>
                    <th >{{translate('Disc Amount')}}</th>
                    <th >{{translate('Dis Type')}}</th>
                    <th >{{translate('Amount From')}}</th>
                    <th >{{translate('Valid from')}}</th>
                    <th >{{translate('Valid to')}}</th>
                    <th >{{translate('Limit use number')}}</th>
                    {{-- <th >{{translate('Created By')}}</th>
                    <th >{{translate('Updated By')}}</th> --}}
                    <th >{{translate('Status')}}</th>
                    
                    <th  width="10%" class="text-center">{{translate('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($coupons as $key => $coupon)
                    
                        <tr>
                            <td  width="3%">{{ ($key+1) + ($coupons->currentPage() - 1)*$coupons->perPage() }}</td>
                            <td width="20%">{{$coupon->name}}</td>
                            <td width="20%">{{$coupon->code}}</td>
                            <td width="20%">{{number_format($coupon->discount_amount, 2)}}</td>
                            <td width="20%">{{$coupon->discount_type}}</td>
                            <td width="20%">{{number_format($coupon->amount_from, 2)}}</td>
                            <td width="20%">{{$coupon->valid_from}}</td>
                            <td width="20%">{{$coupon->valid_to}}</td>
                            <td width="20%">{{$coupon->limit_use_number}}</td>
                            {{-- <td width="20%">{{$coupon->creater->name}}</td>
                            <td width="20%">{{$coupon->updater->name}}</td> --}}
                            <td width="20%">{{$coupon->status}}</td>
                           
                            <td class="text-center d-flex">
		                            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('admin.coupons.edit', $coupon->id)}}" title="{{ translate('Edit') }}">
		                                <i class="las la-edit"></i>
		                            </a>
		                            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('admin.coupons.delete', ['id'=> $coupon->id])}}" title="{{ translate('Delete') }}">
		                                <i class="las la-trash"></i>
		                            </a>
		                        </td>
                        </tr>
               
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $coupons->appends(request()->input())->links() }}
        </div>
    </div>
</div>
{!! hookView('spot-cargo-shipment-client-addon',$currentView) !!}

@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection
