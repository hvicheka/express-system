@extends('backend.layouts.app')

@section('content')
<div class="mx-auto col-lg-12">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Customer Information')}}</h5>
        </div>

        <form class="form-horizontal" action="{{ route('admin.promotions.update',['promotion'=> $promotion->id]) }}" method="POST">
            @csrf
            {{ method_field('PATCH') }}
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Name')}}:</label>
                            <input type="text" id="name" class="form-control" placeholder="{{translate('Name')}}" name="Promotion[name]" value="{{ $promotion->name }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Amount')}}:</label>
                            <input type="number" class="form-control" placeholder="{{translate('Amount')}}" name="Promotion[amount]" value="{{ $promotion->amount }}">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Discount Type')}}:</label>
                            <div class="d-flex">
                                <div class="form-check mr-11">
                                    <input class="form-check-input" type="radio" name="Promotion[discount_type]" value="percentage" id="percentage" {{ $promotion->discount_type == 'percentage' ? 'checked': '' }}>
                                    <label class="form-check-label" for="percentage">%</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="Promotion[discount_type]" value="amount" id="amount" {{ $promotion->discount_type == 'amount' ? 'checked': '' }}>
                                    <label class="form-check-label" for="amount">$</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Promotion Type')}}:</label>
                            <div class="d-flex">
                                <div class="form-check mr-11">
                                    <input class="form-check-input" type="radio" name="Promotion[promotion_type]" value="normal" id="normal" {{ $promotion->promotion_type == 'normal' ? 'checked': '' }}>
                                    <label class="form-check-label" for="normal">Normal</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="Promotion[promotion_type]" value="one time" id="one-time" {{ $promotion->promotion_type == 'one time' ? 'checked': '' }}>
                                    <label class="form-check-label" for="one-time">One Time</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('From Date')}}:</label>
                            <input type="date" class="form-control" placeholder="{{translate('From Date')}}" name="Promotion[from_date]" value={{ $promotion->from_date }}>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('To Date')}}:</label>
                            <input type="date" class="form-control" placeholder="{{translate('To Date')}}" name="Promotion[to_date]" value={{ $promotion->to_date }}>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Status')}}:</label>
                            <div class="d-flex">
                                <div class="form-check mr-11">
                                    <input class="form-check-input" type="radio" name="Promotion[status]" value="active" id="active" {{ $promotion->status == 'active' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="active">Active</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="Promotion[status]" value="inactive" id="inactive" {{ $promotion->status === 'inactive' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inactive">Inactive</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{translate('Description')}}:</label>
                            <textarea rows="5" class="form-control" placeholder="{{translate('Description')}}" name="Promotion[description]">{{ $promotion->description }}</textarea>
                        </div> 
                    </div>
                </div>
                <div class="mb-0 text-right form-group">
                    <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection