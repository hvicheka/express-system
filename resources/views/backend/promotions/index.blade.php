@extends('backend.layouts.app')

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{translate('All Promotions')}}</h1>
		</div>
		<div class="col-md-6 text-md-right">
			<a href="{{ route('admin.promotions.create') }}" class="btn btn-circle btn-info">
				<span>{{translate('Add Promotion')}}</span>
			</a>
		</div>
	</div>
</div>

<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{translate('Promotion')}}</h5>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th  width="3%">#</th>
                    <th >{{translate('Name')}}</th>
                    <th >{{translate('Dis Type')}}</th>
                    <th >{{translate('Pro Type')}}</th>
                    <th >{{translate('From Date')}}</th>
                    <th >{{translate('To Date')}}</th>
                    <th >{{translate('Amount')}}</th>
                    <th >{{translate('Status')}}</th>
                    <th >{{translate('Created By')}}</th>
                    <th >{{translate('Updated By')}}</th>
                    
                    <th  width="10%" class="text-center">{{translate('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($promotions as $key => $promotion)
                    
                        <tr>
                            <td  width="3%">{{ ($key+1) + ($promotions->currentPage() - 1)*$promotions->perPage() }}</td>
                            <td width="20%">{{$promotion->name}}</td>
                            <td width="20%">{{$promotion->discount_type}}</td>
                            <td width="20%">{{$promotion->promotion_type}}</td>
                            <td width="20%">{{$promotion->from_date}}</td>
                            <td width="20%">{{$promotion->to_date}}</td>
                            <td width="20%">{{number_format($promotion->amount, 2)}}</td>
                            <td width="20%">{{$promotion->status}}</td>
                            <td width="20%">{{$promotion->creater->name}}</td>
                            <td width="20%">{{$promotion->updater->name}}</td>
                           
                            <td class="text-center d-flex">
		                            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('admin.promotions.edit', $promotion->id)}}" title="{{ translate('Edit') }}">
		                                <i class="las la-edit"></i>
		                            </a>
		                            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('admin.promotions.delete', ['id'=> $promotion->id])}}" title="{{ translate('Delete') }}">
		                                <i class="las la-trash"></i>
		                            </a>
		                        </td>
                        </tr>
               
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $promotions->appends(request()->input())->links() }}
        </div>
    </div>
</div>
{!! hookView('spot-cargo-shipment-client-addon',$currentView) !!}

@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection
